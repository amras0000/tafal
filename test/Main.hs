module Main(
      main
    ) where

import Criterion.Main
import Data.Complex(realPart)
import Data.Map
import Test.Hspec

import Tafa.Types
import Tafa.Wavelet

main :: IO ()
main = do 
    -- Test specifications
    hspec $ do
        describe "cwt" $ do
            it "identifies a 1Hz Sinusoid" $
                cwt morlet' (sinSamples 1 1) (Time 15, Frequency 1) `shouldSatisfy` aboutR 1
            it "identifies a 5Hz Sinusoid" $
                cwt morlet' (sinSamples 5 1) (Time 15, Frequency 5) `shouldSatisfy` aboutR 1
            it "identifies a 1kHz Sinusoid" $
                cwt morlet' (sinSamples 1000 1) (Time 15, Frequency 1000) `shouldSatisfy` aboutR 1
            it "identifies a 10kHz Sinusoid" $
                cwt morlet' (sinSamples 10000 1) (Time 15, Frequency 10000) `shouldSatisfy` aboutR 1
            it "identifies sinusoids with varying amplitude" $
                (\x -> cwt morlet' (sinSamples 1000 x) (Time 15, Frequency 1000)) <$> [2,6..20] `shouldSatisfy` aboutListR [2,6..20]
            it "identifies noisy sinusoids" $
                cwt morlet' (sampled $ (+) <$> highFreqNoise <*> sinWith 3 1) (Time 15, Frequency 3) `shouldSatisfy` aboutR 1
        describe "icwt" $ do
            it "reconstructs a 100Hz Sinusoid" $
                icwt morlet' (cwt morlet' (sinSamples 100 2)) (Frequency 100) <$> [Time 10, Time 12.1..Time 20] `shouldSatisfy` (aboutList $ (sinWith 100 2) <$> [10,12.1..20])
                
    -- Benchmarking 
    defaultMain [
        bgroup "finding specific tfr values for a 10000Hz signal" $ ((\(t, f) -> bench (show (t,f)) $ whnf (cwt morlet' (sinSamples 100 3)) (t,f)) <$> (cartesian (Time <$> [0, 10, 20]) (Frequency <$> [20, 200, 2000]))),
        bench "inverse cwt for a sample rate of 100 Hz, single frame" $ whnf (icwt morlet' (cwt morlet' (sinSamples 100 3)) (Frequency 100)) (Time 10)
        ]


highFreqNoise :: Double -> Double
highFreqNoise x = 
      ((sin $ cos (x*1000)) * 2)
    + ((cos $ cos (x*1500)) * 3.2)
    + ((sin $ sin (x*980)) * 1.2)

sinSamples :: Double -> Double -> TimeMap Double
sinSamples f a = sampled $ sinWith f a

sinWith :: Double -> Double -> Double -> Double
sinWith frequency amplitude = (*amplitude) . sin . (* tau) . (* frequency)

sampled :: (Double -> Double) -> TimeMap Double
sampled func = TimeMap $ fromAscList $ 
    (\t -> (Time t, func t)) <$> [0, 0.000025..30.0]

about :: (Fractional a, Ord a) => a -> a -> Bool
about a b = abs (a - b) < 0.05

aboutList :: (Fractional a, Ord a) => [a] -> [a] -> Bool
aboutList a b = all (uncurry about) $ zip a b

aboutMap :: (Fractional k, Fractional a, Ord k, Ord a) => Map k a -> Map k a -> Bool
aboutMap a b = go fst && go snd
    where go f = aboutList (f <$> toAscList a) (f <$> toAscList b)

aboutListR :: (RealFloat a) => [a] -> [Response a] -> Bool
aboutListR a b = all (uncurry aboutR) $ zip a b

aboutR :: (RealFloat a) => a -> Response a -> Bool
aboutR a r = about (absR r) a

absR :: (RealFloat a) => Response a -> a
absR (Response a) = realPart $ abs a

tau = 2*pi


cartesian :: [x] -> [y] -> [(x,y)]
cartesian xs ys = [(x,y) | y <- ys, x <- xs]
