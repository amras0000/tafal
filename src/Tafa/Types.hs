{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeSynonymInstances #-}
module Tafa.Types (
      Frequency(Frequency)
    , Response(Response)
    , Time(Time)
    , TimeMap(TimeMap)
    
    , TFR
    , Signal(downSample, toList, between)
    , fromList
    ) where

import Data.Generics.Aliases(orElse)
import Data.Complex(Complex)
import Data.Map.Strict(Map, toAscList, split, lookupGE, lookupLE, empty, singleton, union, unions, lookupMin, lookupMax, insert, splitLookup, mapAccum, mapAccumWithKey, minViewWithKey)
import DSP.Multirate.Halfband(hb_decim)
import DSP.Filter.FIR.Taps(lpf)

newtype Time a = Time a deriving (Eq, Ord, Enum, Num, Fractional, Real, RealFrac)
newtype Frequency a = Frequency a deriving (Eq, Ord, Enum, Fractional, Num, Real, RealFrac, Floating)
newtype Response a = Response (Complex a) deriving (Eq, Num, Show, Fractional)

instance (Show a) => Show (Time a) where
    show (Time a) = show a ++ "s"

instance (Show a) => Show (Frequency a) where
    show (Frequency a) = show a ++ "Hz"

type TFR a = ((Time a, Frequency a) -> Response a)
newtype TimeMap a = TimeMap (Map (Time a) a)

class Signal s where
    downSample :: (Ord a, Floating a) => Frequency a -> s a -> s a
    toList :: (Enum a, Fractional a) => s a -> [(Time a, a)]
    between :: (RealFrac a) => (Time a, Time a) -> s a -> s a
    
instance Signal TimeMap where
    downSample (Frequency f) (TimeMap m) = TimeMap $ mSample (Time $ 1/f) (Time 0) $ mLpf (Frequency f) m
    toList (TimeMap m) = toAscList m
    between b (TimeMap m) = TimeMap $ mBetween b m
    
data LinSignal a = LinSignal {
      samples :: [a]
    , samplingFreq :: Frequency a
    , halfband :: LinSignal a
    , startTime :: Time a
    }

lEmpty :: (Num a) => LinSignal a
lEmpty = LinSignal {samples = mempty, samplingFreq = 0, halfband = lEmpty, startTime = Time 0}

fromList :: (Enum a, RealFrac a, Floating a) => Frequency a -> [a] -> Int -> LinSignal a
fromList sFreq list listLen = LinSignal {
      samples = list
    , samplingFreq = sFreq
    , halfband = halfbandOf sFreq list listLen
    , startTime = Time 0
    }
  where
    halfbandOf f l i = let s = hb_d l i in LinSignal {samples = s, samplingFreq = f/2, halfband = halfbandOf (f/2) s (ceiling $ fromIntegral i/2), startTime = Time 0}
    hb_d l i = hb_decim (lpf (tau/4) i) l
    
instance Signal LinSignal where
    downSample f sig
        | samplingFreq sig < f = lEmpty
        | samplingFreq sig < f*2 = sig
        | otherwise = downSample f $ halfband sig
    toList = lToList
    between = lBetween


lBetween :: (RealFrac a) => (Time a, Time a) -> LinSignal a -> LinSignal a
lBetween (start, end) sig = sig {
      samples = take (ceiling $ (end - start) * Time f) $ drop (floor $ normStart * Time f) $ samples sig
    , halfband = lBetween (start, end) $ halfband sig
    , startTime = start
    }
  where
    Frequency f = samplingFreq sig
    normStart = (start - startTime sig)
    
lToList :: (Enum a, Fractional a) => LinSignal a -> [(Time a, a)]
lToList sig = zip [start, start + Time(1/f)..] $ samples sig
  where
    Frequency f = samplingFreq sig
    start = startTime sig

-- * Map instances

mSample :: (Num k, Ord k) => k -> k -> Map k v -> Map k v
mSample newInterval startingPoint m = 
    case orElse (lookupGE startingPoint m) (lookupLE startingPoint m) of
        Just (k, v) ->
            let (smallMap, greatMap) = split k m
                dsSmall = ds fst newInterval k smallMap
                dsGreat = ds snd newInterval k greatMap
            in unions [dsSmall, singleton k v, dsGreat]
        Nothing -> empty
  where
    ds :: (Num k, Ord k) => (forall a. (a,a) -> a) -> k -> k -> Map k v -> Map k v
    ds locate' interval origin mp =
        let m' = locate' $ split origin mp
            (sm, loc, gr) = splitLookup (locate' (origin - interval, origin + interval)) m'
            newOrigin = case loc of
                Just x -> Just (origin, x)
                Nothing -> (\t -> orElse (fst t) (snd t)) $ (locate' ((lookupMin gr, lookupMax sm), (lookupMax sm, lookupMin gr)))
        in case newOrigin of
            Just (k,v) -> insert k v $ ds locate' interval k $ locate' (sm, gr)
            Nothing -> empty
            

mBetween :: (Ord k) => (k, k) -> Map k v -> Map k v
mBetween (start, end) = fst . split end . snd . split start
            

mLpf :: (Ord a, Floating a) => Frequency a -> Map (Time a) a -> Map (Time a) a
mLpf (Frequency freq) m = case lookupMin m of
    Nothing -> empty
    Just (minKey, val) -> snd $ mapAccum go minKey $ mapDt m
  where
    rc = 1/(tau*freq)
    mapDt m' = case minViewWithKey m' of
        Nothing -> empty
        Just ((k, v), droppedMap) ->
            singleton k (v,0) `union`
            snd (mapAccumWithKey
            (\prevKey k' val -> (k', (val, k'-prevKey))) 
            k droppedMap)
    go (Time avg) (x, (Time dt)) = 
        let alfa = dt/(rc+dt)
        in dup $ (alfa * x) + ((1-alfa) * avg)
    dup a = (Time a, 1.5 * a)

tau :: Floating a => a
tau = 2 * pi
