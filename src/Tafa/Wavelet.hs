

module Tafa.Wavelet (
    -- * Types
      Wavelet
    -- * Wavelet generation
    , morlet
    , morlet'
    -- * Transforms
    , cwt
    , icwt
    ) where

import Data.Complex(Complex((:+)), conjugate, realPart)

import Tafa.Types(Time(Time), Frequency(Frequency), Response(Response), TFR, Signal(..))

-- | The mother wavelet should be normalized to a frequency of 1 Hz
-- the time span is the surroundings of the mother wavelet for which the response is non-negligable
data Wavelet a = Wavelet {
      mother :: Time a -> Response a
    , timeSpan :: (Time a, Time a)
    , dual :: Time a -> Response a
--    , edgeNormalization :: 
    }

morlet :: (RealFloat a) => a -> Wavelet a
morlet bandwidth = Wavelet {
      mother = mother'
    , timeSpan = (Time (-r), Time r)
    , dual = dual'
    }
  where
    r = bandwidth * 2.6
    mother' (Time t) = Response $
        (realToFrac $ 2 / sqrt (pi * bandwidth)) * -- normalization
        (realToFrac $ exp (-(t**2) / bandwidth)) * -- gaussian window
        exp (0 :+ tau * t) -- pure complex exponential at 1Hz
    dual' (Time t) = Response $
        (realToFrac $ 0.5 * pi / sqrt(bandwidth)) * -- inverse normalization
        (realToFrac $ exp (-(t**2) / bandwidth)) * -- gaussian window, same as mother
        exp (0 :+ -tau * t) -- conjugate of the mother
        

morlet' :: (RealFloat a) => Wavelet a
morlet' = morlet 2

-- note: for optimization, the signal map should be constructed with fromAscList or similar, and be as small as possible
cwt :: (RealFloat a, Enum a, Signal s) => Wavelet a -> s a -> TFR a
cwt wavelet signal = \(Time offset, Frequency f) ->
    let croppedSignal = sigStripped <$>
            ( toList
            -- $ downSample (Frequency f * 4)
            $ between
                ((timeSpan wavelet /! (realToFrac f)) +! (realToFrac offset)) -- TODO: consider making (a,a) instance Fractional so we can use / instead of /!
                signal
            )
        scaledWavelet = resStripped . (mother wavelet) . (Time) . (*f) . (\x->x-offset)
    in 
    Response $ (realToFrac f) * mulIntegral croppedSignal scal  edWavelet
  where
    sigStripped (Time t, a) = (t, (realToFrac a))
    resStripped (Response a) = a
    
icwt :: (RealFloat a, Enum a) => Wavelet a -> TFR a -> Frequency a -> Time a -> a
icwt wavelet tfr (Frequency sampleRate) time = 
    let freqSamples = (Frequency . exp) <$> [0,1..log (sampleRate/2)]
        timeSamples freq =
            let dt = realToFrac $ 1/freq*4
            in (\(start, end) -> time : [time+dt, time+(2*dt)..end] ++ [time-dt, time-(2*dt)..start]) $ scaledTimeSpan time freq
        scaledTimeSpan offset freq = (timeSpan wavelet /! (realToFrac freq)) +! (realToFrac offset)
        scaledDualWavelet (Frequency freq) = (dual wavelet) . (*Time freq) . (\x->x-time)
    in  realPart $ deResponse $ integral $ (flip fmap) freqSamples $ \freq ->  -- TODO: remove the need for deResponse by making realPart accept Responses somehow
            (freq,
            mulIntegral ((flip fmap) (timeSamples freq) $ \t -> 
                (t, (freToResp $ freq**2) * tfr (t, freq)))
            (scaledDualWavelet freq))
  where
    deResponse (Response a) = a
    freToResp (Frequency a) = Response (a :+ 0)
     
     

    

(+!) :: (Num a) => (a, a) -> a -> (a, a)
(+!) = acrossTuple (+)

(/!) :: (Fractional a) => (a, a) -> a -> (a, a)
(/!) = acrossTuple (/)

acrossTuple :: (a -> b -> c) -> (a, a) -> b -> (c, c)
acrossTuple func (a, a') arg = (a `func` arg, a' `func` arg)

mulIntegral :: (RealFrac t, Fractional a) => [(t, a)] -> (t -> a) -> a
mulIntegral items func = integral $ zip (times) (zipWith (*) values funcout)
  where
    values = snd <$> items
    times = fst <$> items
    funcout = func <$> times

-- | First Order approximation of an integral on the given list of (input, output) pairs
integral :: (RealFrac t, Fractional a) => [(t, a)] -> a
integral [] = 0
integral items = sum (
        (\((time, item), (time', item')) ->
            let dt = abs (time' - time)
            in (realToFrac dt) * (item + item') / 2)
        <$> zip items (drop 1 items)
    )

tau :: Floating a => a
tau = 2 * pi

--icwt :: Wavelet -> SignalProperties -> TFR a -> Signal a
--icwt = undefined
